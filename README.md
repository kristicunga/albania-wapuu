# ALBANIA WAPUU

Meet the Albania Wapuu, [the official mascot of WordPress](https://wapu.us/) with a twist! Our Wapuu is jamming on a [çifteli](https://en.wikipedia.org/wiki/%C3%87ifteli) and rocking a [plis](https://en.wikipedia.org/wiki/Qeleshe), proudly representing Albania's cultural heritage. This lively and unique character embodies the creative spirit of the Albanian WordPress community.

Feel free to use and share our Wapuu in your WordPress projects, WordCamps, events, and merchandise. Let's spread the joy of WordPress and celebrate our culture together ❤️🖤